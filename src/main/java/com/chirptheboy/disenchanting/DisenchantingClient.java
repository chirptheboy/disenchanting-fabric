package com.chirptheboy.disenchanting;

import com.chirptheboy.disenchanting.networking.DisenchantingMessages;
import com.chirptheboy.disenchanting.screen.DisenchanterScreen;
import com.chirptheboy.disenchanting.screen.DisenchantingScreens;
import net.fabricmc.api.ClientModInitializer;
import net.minecraft.client.gui.screen.ingame.HandledScreens;

public class DisenchantingClient implements ClientModInitializer {

    @Override
    public void onInitializeClient() {
        DisenchantingMessages.registerS2CPackets();
        HandledScreens.register(DisenchantingScreens.DISENCHANTER_SCREEN_HANDLER, DisenchanterScreen::new);
    }
}
