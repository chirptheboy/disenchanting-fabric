package com.chirptheboy.disenchanting.block.entity;

import com.chirptheboy.disenchanting.Disenchanting;
import com.chirptheboy.disenchanting.block.DisenchantingBlocks;
import net.fabricmc.fabric.api.object.builder.v1.block.entity.FabricBlockEntityTypeBuilder;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

public class DisenchantingEntities {
    public static BlockEntityType<DisenchanterBlockEntity> DISENCHANTER;

    public static void registerBlockEntities(){
        DISENCHANTER = Registry.register(Registry.BLOCK_ENTITY_TYPE,
                new Identifier(Disenchanting.MOD_ID, "disenchanter"),
                FabricBlockEntityTypeBuilder.create(DisenchanterBlockEntity::new,
                        DisenchantingBlocks.DISENCHANTER).build(null));
    }
}
