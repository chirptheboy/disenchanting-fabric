package com.chirptheboy.disenchanting.block.entity;

import com.chirptheboy.disenchanting.networking.DisenchantingMessages;
import com.chirptheboy.disenchanting.screen.DisenchanterScreenHandler;
import net.fabricmc.fabric.api.networking.v1.PacketByteBufs;
import net.fabricmc.fabric.api.networking.v1.PlayerLookup;
import net.fabricmc.fabric.api.networking.v1.ServerPlayNetworking;
import net.fabricmc.fabric.api.screenhandler.v1.ExtendedScreenHandlerFactory;
import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.Inventories;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.screen.PropertyDelegate;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.text.Text;
import net.minecraft.util.collection.DefaultedList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;

public class DisenchanterBlockEntity extends BlockEntity implements ExtendedScreenHandlerFactory, ImplementedInventory {

    private final DefaultedList<ItemStack> inventory = DefaultedList.ofSize(3, ItemStack.EMPTY);

    protected final PropertyDelegate propertyDelegate;
    private int cost = -1;
    private int blacklisted = 0;
    private int hasenoughxp = 0;

    public void setInventory(DefaultedList<ItemStack> inventory) {
        for (int i = 0; i < inventory.size(); i++) {
            this.inventory.set(i, inventory.get(i));
        }
    }

    @Override
    public void markDirty() {
        if(!world.isClient()) {
            PacketByteBuf data = PacketByteBufs.create();
            data.writeInt(inventory.size());
            for (int i = 0; i < inventory.size(); i++) {
                data.writeItemStack(inventory.get(i));
            }
            data.writeBlockPos(getPos());

            for (ServerPlayerEntity player : PlayerLookup.tracking((ServerWorld) world, getPos())) {
                ServerPlayNetworking.send(player, DisenchantingMessages.ITEM_SYNC, data);
            }
        }
        super.markDirty();
    }

    public DisenchanterBlockEntity(BlockPos pos, BlockState state) {
        super(DisenchantingEntities.DISENCHANTER, pos, state);
        this.propertyDelegate = new PropertyDelegate() {
            public int get(int index) {
                return switch (index) {
                    case 0 -> DisenchanterBlockEntity.this.cost;
                    case 1 -> DisenchanterBlockEntity.this.blacklisted;
                    case 2 -> DisenchanterBlockEntity.this.hasenoughxp;
                    default -> 0;
                };
            }

            public void set(int index, int value) {
                switch (index) {
                    case 0 -> DisenchanterBlockEntity.this.cost = value;
                    case 1 -> DisenchanterBlockEntity.this.blacklisted = value;
                    case 2 -> DisenchanterBlockEntity.this.hasenoughxp = value;
                }
            }
            public int size() {
                return 3;
            }
        };
    }

    @Override
    public void readNbt(NbtCompound nbt) {
        Inventories.readNbt(nbt, inventory);
        super.readNbt(nbt);
        cost = nbt.getInt("disenchanter.cost");
        blacklisted = nbt.getInt("disenchanter.blacklisted");
        hasenoughxp = nbt.getInt("disenchanter.hasenoughxp");
    }

    @Override
    protected void writeNbt(NbtCompound nbt) {
        super.writeNbt(nbt);
        Inventories.writeNbt(nbt, inventory);
        nbt.putInt("disenchanter.cost", cost);
        nbt.putInt("disenchanter.blacklisted", blacklisted);
        nbt.putInt("disenchanter.hasenoughxp", hasenoughxp);
    }

    @Override
    public DefaultedList<ItemStack> getItems() {
        return this.inventory;
    }

    @Override
    public Text getDisplayName() {
        return Text.translatable("Disenchanter");
    }

    @Override
    public void onOpen(PlayerEntity player) {
        ImplementedInventory.super.onOpen(player);
    }

    @Override
    public void writeScreenOpeningData(ServerPlayerEntity player, PacketByteBuf buf) {
        buf.writeBlockPos(this.pos);
    }

    @Nullable
    @Override
    public ScreenHandler createMenu(int syncId, PlayerInventory inv, PlayerEntity player) {
        return new DisenchanterScreenHandler(syncId, inv, this, this.propertyDelegate);
    }

    public static  void tick(World world, BlockPos blockPos, BlockState blockState, DisenchanterBlockEntity entity) {
    }

    @Override
    public boolean canInsert(int slot, ItemStack stack, @Nullable Direction side) {
        return false; //todo can be updated to allow enchanted items in slot 0, books in slot 1, and nothing in slot 2
    }

    @Override
    public boolean canExtract(int slot, ItemStack stack, Direction side) {
        return false;
    }
}
