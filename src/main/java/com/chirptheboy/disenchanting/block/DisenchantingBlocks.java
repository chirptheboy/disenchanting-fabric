package com.chirptheboy.disenchanting.block;

import com.chirptheboy.disenchanting.Disenchanting;
import com.chirptheboy.disenchanting.item.ModItemGroup;
import net.fabricmc.fabric.api.item.v1.FabricItemSettings;
import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.minecraft.block.Block;
import net.minecraft.block.Material;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

public class DisenchantingBlocks {
    public static final Block DISENCHANTER = registerBlock("disenchanter",
            new DisenchanterBlock(FabricBlockSettings.of(Material.STONE).luminance(state -> 7).strength(2.0f).nonOpaque()), ModItemGroup.DISENCHANTING);

    private static Block registerBlock(String name, Block block, ItemGroup tab){
        registerBlockItem(name, block, tab);
        return Registry.register(Registry.BLOCK, new Identifier(Disenchanting.MOD_ID, name), block);
    }

    private static Item registerBlockItem(String name, Block block, ItemGroup tab) {
        return Registry.register(Registry.ITEM, new Identifier(Disenchanting.MOD_ID, name),
                new BlockItem(block, new FabricItemSettings().group(tab)));
    }

    public static void registerBlocks(){}
}
