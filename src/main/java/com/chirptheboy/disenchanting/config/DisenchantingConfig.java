/**
 * Written by MattCzyr for NaturesCompass (https://github.com/MattCzyr/NaturesCompass)
 * Licensed under the Creative Commons Attribution-NonCommercial ShareAlike 4.0 International License.
 * https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode
 */

package com.chirptheboy.disenchanting.config;

import com.chirptheboy.disenchanting.Disenchanting;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import net.fabricmc.loader.api.FabricLoader;

import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Path;

public class DisenchantingConfig {

    private static Path configFilePath;
    private static Gson gson = new GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create();

    public static boolean requiresExperience = true;
    public static boolean randomEnchantment = false;
    public static boolean damageItem = false;
    public static boolean resetCost = false;
    public static int costSlider = 2;
    public static int damagePercent = 5;

    public static void registerConfig() {
        Reader reader;
        if(getFilePath().toFile().exists()) {
            try {
                reader = Files.newBufferedReader(getFilePath());
                Data data = gson.fromJson(reader, Data.class);

                requiresExperience = data.common.requiresExperience;
                randomEnchantment = data.common.randomEnchantment;
                damageItem = data.common.damageItem;
                resetCost = data.common.resetCost;
                costSlider = data.common.costSlider;
                damagePercent = data.common.damagePercent;

                reader.close();

            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        save();
    }

    public static void save() {
        try {
            Writer writer = Files.newBufferedWriter(getFilePath());
            Data data = new Data(new Data.Common(requiresExperience, randomEnchantment, damageItem, resetCost, costSlider, damagePercent));
            gson.toJson(data, writer);
            writer.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static Path getFilePath() {
        if(configFilePath == null) {
            configFilePath = FabricLoader.getInstance().getConfigDir().resolve(Disenchanting.MOD_ID + ".json");
        }
        return configFilePath;
    }

    private static class Data {

        private Common common;

        public Data(Common common) {
            this.common = common;
        }

        private static class Common {
            private final String requiresExperienceComment = "Does disenchanting require experience?";
            private final boolean requiresExperience;

            private final String randomEnchantmentComment = "Should the disenchanter select the a random enchantment on the enchanted item instead of the first one?";
            private final boolean randomEnchantment;

            private final String damageItemComment = "Should the enchanted item be damaged during the disenchanting process?";
            private final boolean damageItem;

            private final String resetCostComment = "Should the enchanted item's anvil work cost be reset to zero when fully disenchanted?";
            private final boolean resetCost;

            private final String costSliderComment = "Sliding multiplier for the experience cost. Default is 3.";
            private final int costSlider;

            private final String damagePercentComment = "Percent of damage that should be done to the item on each disenchant. Default is 5.";
            private final int damagePercent;

            private Common() {
                requiresExperience = true;
                randomEnchantment = false;
                damageItem = false;
                resetCost = false;
                costSlider = 3;
                damagePercent = 5;
            }

            private Common(boolean requiresExperience, boolean randomEnchantment, boolean damageItem, boolean resetCost, int costSlider, int damagePercent) {
                this.requiresExperience = requiresExperience;
                this.randomEnchantment = randomEnchantment;
                this.damageItem = damageItem;
                this.resetCost = resetCost;
                this.costSlider = costSlider;
                this.damagePercent = damagePercent;
            }
        }
    }
}