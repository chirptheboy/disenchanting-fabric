package com.chirptheboy.disenchanting.item;

import com.chirptheboy.disenchanting.Disenchanting;
import com.chirptheboy.disenchanting.block.DisenchantingBlocks;
import net.fabricmc.fabric.api.client.itemgroup.FabricItemGroupBuilder;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Identifier;

public class ModItemGroup {
    public static final ItemGroup DISENCHANTING = FabricItemGroupBuilder.build(
            new Identifier(Disenchanting.MOD_ID, "disenchanting"), () -> new ItemStack(DisenchantingBlocks.DISENCHANTER.asItem()));
}
