package com.chirptheboy.disenchanting;

import com.chirptheboy.disenchanting.block.DisenchantingBlocks;
import com.chirptheboy.disenchanting.block.entity.DisenchantingEntities;
import com.chirptheboy.disenchanting.config.DisenchantingConfig;
import com.chirptheboy.disenchanting.data.DisenchantingTags;
import com.chirptheboy.disenchanting.networking.DisenchantingMessages;
import com.chirptheboy.disenchanting.screen.DisenchantingScreens;
import net.fabricmc.api.ModInitializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Disenchanting implements ModInitializer {
	public static final String MOD_ID = "disenchanting";
	public static final Logger LOGGER = LoggerFactory.getLogger(MOD_ID);

	@Override
	public void onInitialize() {
		DisenchantingConfig.registerConfig();
		DisenchantingBlocks.registerBlocks();
		DisenchantingEntities.registerBlockEntities();
		DisenchantingScreens.registerScreenHandlers();
		DisenchantingTags.registerTags();
	}
}
