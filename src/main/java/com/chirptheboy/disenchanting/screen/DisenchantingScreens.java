package com.chirptheboy.disenchanting.screen;

import com.chirptheboy.disenchanting.Disenchanting;
import net.fabricmc.fabric.api.screenhandler.v1.ExtendedScreenHandlerType;
import net.minecraft.screen.ScreenHandlerType;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

public class DisenchantingScreens {
    public static ScreenHandlerType<DisenchanterScreenHandler> DISENCHANTER_SCREEN_HANDLER =
            new ExtendedScreenHandlerType<>(DisenchanterScreenHandler::new);

    public static void registerScreenHandlers(){
        Registry.register(Registry.SCREEN_HANDLER, new Identifier(Disenchanting.MOD_ID, "disenchanting"), DISENCHANTER_SCREEN_HANDLER);
    }
}
