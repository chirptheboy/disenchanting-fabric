package com.chirptheboy.disenchanting.screen;

import com.chirptheboy.disenchanting.Disenchanting;
import com.mojang.blaze3d.systems.RenderSystem;
import net.minecraft.client.gui.screen.ingame.HandledScreen;
import net.minecraft.client.render.GameRenderer;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;

import java.awt.*;

public class DisenchanterScreen extends HandledScreen<DisenchanterScreenHandler> {
    private static final Identifier DISENCHANTER_GUI =
            new Identifier(Disenchanting.MOD_ID, "textures/gui/disenchanter.png");

    private static final Identifier DISENCHANTER_GUI_DISALLOWED =
            new Identifier(Disenchanting.MOD_ID, "textures/gui/disenchanter_disallowed.png");

    public DisenchanterScreen(DisenchanterScreenHandler handler, PlayerInventory inventory, Text title) {
        super(handler, inventory, title);
    }

    @Override
    protected void init() {
        super.init();
    }

    @Override
    protected void drawBackground(MatrixStack matrices, float delta, int mouseX, int mouseY) {
        RenderSystem.setShader(GameRenderer::getPositionTexShader);
        RenderSystem.setShaderColor(1.0F,1.0F,1.0F,1.0F);
        RenderSystem.setShaderTexture(0, handler.isBlacklisted() ? DISENCHANTER_GUI_DISALLOWED : DISENCHANTER_GUI);
        int x = (width - backgroundWidth) / 2;
        int y = (height - backgroundHeight) / 2;
        drawTexture(matrices, x, y, 0, 0, backgroundWidth, backgroundHeight);
    }

    @Override
    protected void drawForeground(MatrixStack matrices, int mouseX, int mouseY) {

        int cost = handler.getCost();
        boolean hasEnoughXP = handler.hasEnoughXP();

        String strLabel;
        String strCost;
        int color = 4210752;

        if (cost == -1) {
            strLabel = strCost = "";
        } else {
            strLabel = "Cost: ";
            strCost = "" + cost;
            color = hasEnoughXP ? 4210752 : 0xbf634f;
        }

        this.textRenderer.draw(matrices, strLabel, (float)this.titleX, (float)this.titleY, 4210752);
        this.textRenderer.draw(matrices, strCost, (float)this.titleX + textRenderer.getWidth(strLabel), (float)this.titleY, color);
    }

    @Override
    public void render(MatrixStack matrices, int mouseX, int mouseY, float delta) {
        renderBackground(matrices);
        super.render(matrices, mouseX, mouseY, delta);
        drawMouseoverTooltip(matrices, mouseX, mouseY);
    }
}
