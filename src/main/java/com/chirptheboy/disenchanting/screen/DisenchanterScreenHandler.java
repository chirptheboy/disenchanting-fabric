package com.chirptheboy.disenchanting.screen;

import com.chirptheboy.disenchanting.config.DisenchantingConfig;
import com.chirptheboy.disenchanting.data.DisenchantingTags;
import com.google.common.collect.ImmutableMap;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.Inventory;
import net.minecraft.item.EnchantedBookItem;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.screen.ArrayPropertyDelegate;
import net.minecraft.screen.PropertyDelegate;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.screen.slot.Slot;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.util.registry.Registry;

import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

public class DisenchanterScreenHandler extends ScreenHandler {
    private final Inventory inventory;
    private final PlayerEntity player;
    private final PropertyDelegate propertyDelegate;
    private final int SLOT_ITEM = 0;
    private final int SLOT_BOOK = 1;
    private final int SLOT_OUTPUT = 2;
    private final int YES = 1;
    private final int NO = 0;
    private final int PROPERTY_COST = 0;
    private final int PROPERTY_HAS_ENOUGH_XP = 1;
    private final int PROPERTY_IS_BLACKLISTED = 2;

    public DisenchanterScreenHandler(int syncId, PlayerInventory inventory, PacketByteBuf buf){
        this(syncId, inventory, inventory.player.getWorld().getBlockEntity(buf.readBlockPos()), new ArrayPropertyDelegate(3));
    }

    public DisenchanterScreenHandler(int syncId, PlayerInventory playerInventory, BlockEntity entity, PropertyDelegate delegate) {
        super(DisenchantingScreens.DISENCHANTER_SCREEN_HANDLER, syncId);
        checkSize(((Inventory) entity), 3);
        this.inventory = (Inventory) entity;
        this.player = playerInventory.player;
        this.propertyDelegate = delegate;
        inventory.onOpen(playerInventory.player);

       /** Add enchanted item slot */
        this.addSlot(new Slot(inventory, SLOT_ITEM, 29, 25){
            @Override
            public boolean canInsert(ItemStack stack) {
                return EnchantmentHelper.get(stack).size() > 0 && !stack.isIn(DisenchantingTags.DISENCHANTER_IMMUNE);
            }

            @Override
            public void onTakeItem(PlayerEntity player, ItemStack stack) {
                super.onTakeItem(player, stack);
                clearOutputSlot();
                this.markDirty();
            }

            @Override
            public void setStack(ItemStack stack) {
                super.setStack(stack);
                tryCreateOutputItem();
                this.markDirty();
            }
        });

        /** Add book slot */
        this.addSlot(new Slot(inventory, SLOT_BOOK, 83, 25){
            @Override
            public boolean canInsert(ItemStack stack) {
                return stack.getItem() == Items.BOOK;
            }

            @Override
            public void onTakeItem(PlayerEntity player, ItemStack stack) {
                super.onTakeItem(player, stack);
                if(!inputSlotsValid()){
                    clearOutputSlot();
                }
                this.markDirty();
            }

            @Override
            public void setStack(ItemStack stack) {
                super.setStack(stack);
                tryCreateOutputItem();
                this.markDirty();
            }
        });

        /** Add enchanted book output slot */
        this.addSlot(new Slot(inventory, SLOT_OUTPUT, 137, 25){
            @Override
            public boolean canInsert(ItemStack stack) {
                return false;
            }

            @Override
            public void onTakeItem(PlayerEntity player, ItemStack stack) {
                disenchantItem(stack);
                super.onTakeItem(player, stack);
                this.markDirty();
            }
        });

        layoutPlayerInventorySlots(playerInventory, 11, 71);
        addProperties(delegate);
    }

    @Override
    public void close(PlayerEntity player) {
        clearOutputSlot();
        super.close(player);
    }

    public void disenchantItem(ItemStack outputEnchantedBook){
        if (!player.world.isClient()){
            boolean itemBroke = false;
            Slot ItemSlot = this.getSlot(SLOT_ITEM);
            Slot BookSlot = this.getSlot(SLOT_BOOK);

            ItemStack inputEnchantedItem = ItemSlot.getStack();

            /** Deduct experience if required */
            if (DisenchantingConfig.requiresExperience && !player.isCreative()) {
                player.addExperienceLevels(-this.propertyDelegate.get(PROPERTY_COST));
            }

            /** Since the itemStack is the newly-enchanted book, it should only have one enchantment */
            Enchantment enchantmentToRemove = getEnchantmentFromItemStack(outputEnchantedBook, 0);
            Map<Enchantment, Integer> enchantmentMap = EnchantmentHelper.get(inputEnchantedItem);
            int numEnchantments = enchantmentMap.size();
            enchantmentMap.remove(enchantmentToRemove);

            /** Create a copy of the input item as the output item to go in its place. Using 'copy' to get the damage,
             *  then updating the repair cost if the config is set AND it's the last enchantment being removed. */
            ItemStack outputItem = inputEnchantedItem.copy();
            if (DisenchantingConfig.resetCost && numEnchantments == 1) {
                outputItem.setRepairCost(0);
            }
            EnchantmentHelper.set(enchantmentMap, outputItem);

            /** Damage or destroy the item if configured to */
            if (DisenchantingConfig.damageItem && outputItem.isDamageable()) {
                float durabilityToReduceBy = (float)outputItem.getMaxDamage() * ((DisenchantingConfig.damagePercent / 100.0F));
                int currentDurability = outputItem.getMaxDamage() - outputItem.getDamage();
                int newDamage = outputItem.getDamage() + (int)durabilityToReduceBy;
                int newDurability = currentDurability - (int)durabilityToReduceBy;

                /** If damaged too much, its destroyed */
                if (newDurability < 1) {
                    outputItem = new ItemStack(Items.AIR);
                    itemBroke = true;
                } else {
                    outputItem.setDamage(newDamage);
                }
            }

            /** Override the output for books */
            if (outputItem.isOf(Items.ENCHANTED_BOOK)) {
                if (enchantmentMap.size() == 0)
                    outputItem = new ItemStack(Items.BOOK);
                else {
                    outputItem = new ItemStack(Items.ENCHANTED_BOOK);
                    EnchantmentHelper.set(enchantmentMap, outputItem);
                }
            }

            /** Play the sound */
            this.player.world.playSound((PlayerEntity)null, this.player.getBlockPos(), itemBroke ? SoundEvents.ENTITY_ITEM_BREAK : SoundEvents.BLOCK_ENCHANTMENT_TABLE_USE, SoundCategory.BLOCKS, 1.0F, this.player.world.random.nextFloat() * 0.1F + 0.9F);

            /** Update the input and output slots */
            BookSlot.takeStack(1);
            ItemSlot.setStack(outputItem);
        }
    }

    public void tryCreateOutputItem(){
        if (!player.world.isClient){
            ItemStack inputStack = this.getSlot(SLOT_ITEM).getStack();

            int VAL_NO_COST = -1;
            float COST_REDUCER = 0.35F;
            float COST_SPREADER = 4F;

            /** If both input slots are valid */
            if (inputSlotsValid()){
                Enchantment selectedEnchantment;
                int selectedEnchantmentLevel = 0;
                int cost = 0;
                int slider = DisenchantingConfig.costSlider;
                boolean hasEnoughXP = false;


                /** Determine the enchantment to use */
                int enchantmentNumber = DisenchantingConfig.randomEnchantment ? ThreadLocalRandom.current().nextInt(0, EnchantmentHelper.get(inputStack).size()) : 0;
                selectedEnchantment = getEnchantmentFromItemStack(inputStack, enchantmentNumber);
                selectedEnchantmentLevel = inputStack.isOf(Items.ENCHANTED_BOOK) ?
                        EnchantmentHelper.getLevelFromNbt(EnchantedBookItem.getEnchantmentNbt(inputStack).getCompound(enchantmentNumber)) :
                        EnchantmentHelper.getLevel(selectedEnchantment, inputStack);

                /** Only continue if the enchantment isn't on the blacklist */
                if(!Registry.ENCHANTMENT.entryOf(Registry.ENCHANTMENT.getKey(selectedEnchantment).orElse(null)).isIn(DisenchantingTags.DISENCHANTER_BLACKLIST)) {

                    this.propertyDelegate.set(PROPERTY_IS_BLACKLISTED, NO);

                    /** Only calculate cost if required */
                    if (DisenchantingConfig.requiresExperience && !player.isCreative()) {
                        int rarity = selectedEnchantment.getRarity().getWeight();
                        cost = (int) Math.max(((selectedEnchantmentLevel + invertRarity(rarity)) * COST_REDUCER) * (((float) slider / 2) + (slider * slider) / COST_SPREADER),1);
                        hasEnoughXP = player.experienceLevel >= cost;
                        this.propertyDelegate.set(PROPERTY_COST, cost);
                        this.propertyDelegate.set(PROPERTY_HAS_ENOUGH_XP, hasEnoughXP ? YES : NO);
                    } else {
                        this.propertyDelegate.set(PROPERTY_COST, VAL_NO_COST);
                    }

                    /** Create the enchanted book after the following checks:
                     * player is in creative OR you have enough experience OR experience is not required */
                    if (!DisenchantingConfig.requiresExperience || (DisenchantingConfig.requiresExperience && hasEnoughXP) || player.isCreative()) {
                        ItemStack enchantedBook = new ItemStack(Items.ENCHANTED_BOOK);
                        EnchantmentHelper.set(ImmutableMap.of(selectedEnchantment, selectedEnchantmentLevel), enchantedBook);
                        this.getSlot(SLOT_OUTPUT).setStack(enchantedBook);
                    } else {
                        clearOutputSlot();
                    }
                } else {
                    this.propertyDelegate.set(PROPERTY_IS_BLACKLISTED, YES);
                    clearOutputSlot();
                }

            /** Both input slots are not valid */
            } else {
                clearOutputSlot();
                this.propertyDelegate.set(PROPERTY_COST, VAL_NO_COST); // -1 becomes blank int, -2 becomes blank cost string
                this.propertyDelegate.set(PROPERTY_IS_BLACKLISTED, NO);
            }
        }
    }

    public boolean inputSlotsValid(){
        return this.getSlot(SLOT_BOOK).getStack().isOf(Items.BOOK) && isEnchantedItem(this.getSlot(SLOT_ITEM).getStack());
    }

    public int invertRarity(int rarity){
        return switch (rarity) {
            case 10 -> 1;
            case 5 -> 2;
            case 2 -> 5;
            case 1 -> 10;
            default -> 0;
        };
    }

    public int getCost(){
        return this.propertyDelegate.get(PROPERTY_COST);
    }

    public boolean isBlacklisted(){
        return this.propertyDelegate.get(PROPERTY_IS_BLACKLISTED) == YES;
    }

    public boolean hasEnoughXP(){
        return this.propertyDelegate.get(PROPERTY_HAS_ENOUGH_XP) == YES;
    }

    public void clearOutputSlot(){
        this.getSlot(SLOT_OUTPUT).setStack(ItemStack.EMPTY);
    }

    public boolean isEnchantedItem(ItemStack stack){
        return EnchantmentHelper.get(stack).size() > 0;
    }

    public Enchantment getEnchantmentFromItemStack(ItemStack enchantedItem, int enchantmentNumber) {
        Enchantment selectedEnchantment = null;
        Map<Enchantment, Integer> enchantmentMap = EnchantmentHelper.get(enchantedItem);
        Iterator<Map.Entry<Enchantment, Integer>> enchantmentIterator = enchantmentMap.entrySet().iterator();

        /** Loop through the enchantment count until the random number is hit, then set the enchantment */
        for (int loopCounter = 0; loopCounter <= enchantmentNumber; loopCounter++){
            Map.Entry<Enchantment, Integer> entry = enchantmentIterator.next();
            if(loopCounter == enchantmentNumber) {
                selectedEnchantment = entry.getKey();
                break;
            }
        }
        return selectedEnchantment;
    }

    @Override
    public ItemStack transferSlot(PlayerEntity player, int invSlot) {
        ItemStack newStack = ItemStack.EMPTY;
        ItemStack extractedItem = this.slots.get(invSlot).getStack().copy();
        Slot slot = this.slots.get(invSlot);
        if (slot != null && slot.hasStack()) {
            ItemStack originalStack = slot.getStack();
            newStack = originalStack.copy();
            if (invSlot < this.inventory.size()) {
                if (!this.insertItem(originalStack, this.inventory.size(), this.slots.size(), true)) {
                    return ItemStack.EMPTY;
                }
            } else if (!this.insertItem(originalStack, 0, this.inventory.size(), false)) {
                return ItemStack.EMPTY;
            }

            if (originalStack.isEmpty()) {
                slot.setStack(ItemStack.EMPTY);
                if (invSlot == SLOT_OUTPUT) {
                    disenchantItem(extractedItem);
                }
            } else {
                slot.markDirty();
            }
        }
        return newStack;
    }

    @Override
    public boolean canUse(PlayerEntity player) {
        return this.inventory.canPlayerUse(player);
    }

    protected void layoutPlayerInventorySlots(PlayerInventory playerInventory, int leftCol, int topRow) {
        // Player inventory
        addSlotBox(playerInventory, 9, leftCol, topRow, 9, 18, 3, 18);
        // Hotbar
        topRow += 58;
        addSlotRange(playerInventory, 0, leftCol, topRow, 9, 18);
    }

    private int addSlotRange(Inventory handler, int index, int x, int y, int amount, int dx) {
        for (int i = 0; i < amount; i++) {
            addSlot(new Slot(handler, index, x, y));
            x += dx;
            index++;
        }
        return index;
    }

    private int addSlotBox(Inventory handler, int index, int x, int y, int horAmount, int dx, int verAmount, int dy) {
        for (int j = 0; j < verAmount; j++) {
            index = addSlotRange(handler, index, x, y, horAmount, dx);
            y += dy;
        }
        return index;
    }
}
