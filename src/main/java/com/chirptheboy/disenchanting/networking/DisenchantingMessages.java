package com.chirptheboy.disenchanting.networking;

import com.chirptheboy.disenchanting.Disenchanting;
import net.fabricmc.fabric.api.client.networking.v1.ClientPlayNetworking;
import net.minecraft.util.Identifier;

public class DisenchantingMessages {
    public static final Identifier ITEM_SYNC = new Identifier(Disenchanting.MOD_ID, "item_sync");

    public static void registerS2CPackets(){
        ClientPlayNetworking.registerGlobalReceiver(ITEM_SYNC, ItemStackSyncS2CPacket::receive);
    }
}
