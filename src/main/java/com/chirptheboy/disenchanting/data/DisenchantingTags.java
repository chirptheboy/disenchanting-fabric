package com.chirptheboy.disenchanting.data;

import com.chirptheboy.disenchanting.Disenchanting;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.item.Item;
import net.minecraft.tag.TagKey;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;


public class DisenchantingTags {

    public static final TagKey<Item> DISENCHANTER_IMMUNE = TagKey.of(Registry.ITEM_KEY, new Identifier(Disenchanting.MOD_ID, "disenchanter_immune"));

    public static final TagKey<Enchantment> DISENCHANTER_BLACKLIST = TagKey.of(Registry.ENCHANTMENT.getKey(), new Identifier(Disenchanting.MOD_ID, "blacklisted_enchantments"));

    public static void registerTags(){
        // do not delete:! this makes the mod get classloaded so the wrapper tags correctly get added to the registry early, before recipe testing
    }
}
