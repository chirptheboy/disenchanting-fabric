# Disenchanting Fabric

Disenchanting is a Fabric port of [DisenchantingForge](https://gitlab.com/chirptheboy/disenchanting).

This mod adds a Disenchanter block that will let you disenchant enchanted items and transfer their enchantments onto books. The Disenchanter will remove the first enchantment from the item and transfer it onto a book. If you shift-click the output book, the Disenchanter will try to transfer all enchantments onto all available books, assuming you have enough experience and books to cover the cost. You can use the random_enchantment config to pull a specific enchantment off an item by placing and re-placing it in the input slot until the desired enchanted book appears in the output. Actually choosing the enchantment is on the roadmap.

# Download

[Download on CurseForge](https://www.curseforge.com/minecraft/mc-mods/disenchantingfabric)

# Recipe
- 1 Anvil
- 1 Enchanting table
- 2 Gold ingots
- 3 Obsidian blocks

![alt text](web/recipe.png "Disenchanter recipe")

# How to use
1. Put an enchanted item into the left slot
2. Place a book or stack of books into the middle slot
3. Enchanted book will generate with the first enchantment from the enchanted item
4. Taking the book will remove that enchantment from the item and give you the book

# Notes
- Config options are set at the server level, so any client-side values are ignored when playing on a server.

# Config options
1. `randomEnchantment` - Should the disenchanter select a random enchantment on the enchanted item instead of the first one? (Default: `FALSE`)
2. `requiresExperience` - Does disenchanting require experience? (Default: `TRUE`)
3. `costSlider` - Sliding multiplier for the experience cost. Range is 1 - 10. (Default `2`)
4. `resetAnvilCost` - Should the enchanted item's anvil work cost be reset to zero when fully disenchanted? (Default: `FALSE`)
5. `damageItem` - Should the enchanted item be damaged during the disenchanting process? (Default: `FALSE`). Added in 2.2.0.
6. `damagePercent` - Percent of damage that should be done to the item on each disenchant. Range is 1 - 100. (Default 5). Added in 2.2.0.

# Known bugs
- When accessing the Disenchanter's UI, shift-clicking doesn't move items between the hotbar and player inventory.
- Shift-clicking the enchanted book result to remove it doesn't generate the next one automatically (assuming one could be created).
- Please log bugs on the issue tracker [here](https://gitlab.com/chirptheboy/disenchanting/-/issues/new)

# BDCraft Packs
- [Disenchanting 128x](https://gitlab.com/chirptheboy/disenchanting/-/raw/master/packs/Disenchanter-1.14-128x.zip?inline=false)
- [Disenchanting 256x](https://gitlab.com/chirptheboy/disenchanting/-/raw/master/packs/Disenchanter-1.14-256x.zip?inline=false)
- [Disenchanting 512x](https://gitlab.com/chirptheboy/disenchanting/-/raw/master/packs/Disenchanter-1.14-512x.zip?inline=false)

# Special thanks
- [KaupenJoe](https://youtube.com/@kaupenjoe) for his Fabric modding tutorials
- [akharding91](https://gitlab.com/akharding91) for the BDCraft textures
